"""
:mod:`association` module : un module pour la modélisation des blocs

:author: Roman Diez Berzal

:date: 2024 mars

"""

from PIL import Image, ImageDraw
from manip_couleurs import *
    
class Bloc:
    """Classe d'un bloc d'une image en pixel
    """

    def __init__(self, image: Image):
        """
        Initialise le type bloc à la valeur de l'image 
        """
        largeur, hauteur = image.size
        pixel_hl = image.getpixel((0, 0))
        pixel_lr = image.getpixel((largeur, hauteur))
        self.image = image
        
    def __eq__(self, other):
        """
        Renvoie True si 2 images sont identiques
        Précondition : aucune
        Exemple(s) :
        $$$ 

        """
        return self.image == other.image
    
    def est_bloc_uniforme(bloc:Bloc)->bool:
        """
        Renvoie True si le bloc est uniforme
        Précondition : aucune
        Exemple(s) :
        $$$ 

        """
        return bloc.pixel_hl == bloc.pixel_lr
    
    
    def est_bloc_non_uniforme()-> bool:
        """
        Renvoie True si le bloc n'est pas uniforme
        Précondition : aucune
        Exemple(s) :
        $$$ 

        """
        return not(est_bloc_uniforme())
    
    def sont_4_blocs_uniformes_proches(bloc1: Bloc, bloc2: Bloc, bloc3: Bloc, bloc4: Bloc)-> bool:
        """
        Renvoie True si les 4 blocs uniformes sont proches
        Précondition : aucune
        Exemple(s) :
        $$$ sont_4_blocs_uniformes_proches(bloc1, bloc2, bloc3, bloc4)
        True
        $$$ sont_4_blocs_uniformes_proches(bloc1, bloc2, bloc3, bloc4)
        False
        """
        res = False
        parametres = [bloc1, bloc2, bloc3, bloc4]
        if all(est_bloc_uniforme(parametres)):
            for i in parametres(1,len(parametres)-1):
                if est_couleur_proche(bloc1, bloc[i]):
                    res = True
        return res
