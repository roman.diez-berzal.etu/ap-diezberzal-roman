"""
:mod:`fonctions` module : fonctions pour la résolution du projet

:author: Roman Diez Berzal

:date: 2024 mars

"""

def moyenne_couleur_bloc(liste: list[tuple[int,int,int]]) -> tuple:
    """
    Renvoie la moyenne de la couleur d'un bloc
    Précondition : aucune
    Exemple(s) :
    $$$ moyenne_couleur_bloc([(1,2,3),(4,5,6),(7,8,9)])
    (4,5,6)
    """
    total_r = 0
    total_g = 0
    total_b = 0
    for couleur in liste:
        total_r += couleur[0]
        total_g += couleur[1]
        total_b += couleur[2]
    nb_blocs = len(liste)
    moyenne_r = total_r / nb_blocs
    moyenne_g = total_g / nb_blocs
    moyenne_b = total_b / nb_blocs
    return (moyenne_r, moyenne_g, moyenne_b)

def est_couleur_proche(couleur1 : tuple[int,int,int], couleur2 : tuple[int,int,int]) -> bool:
    """
    Renvoie True si 2 couleurs sont proches
    Précondition : len(couleur1) == len(couleur2) == 3
    Exemple(s) :
    $$$ est_couleur_proche((50, 55, 60), (52, 57, 61))
    True
    $$$ est_couleur_proche((12, 13, 14), (125, 125, 137))
    False
    """
    res = True
    for i in range(len(couleur1)):
        if abs(couleur1[i] - couleur2[i]) > 5:
            res = False
    return res
