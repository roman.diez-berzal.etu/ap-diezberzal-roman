"""
:mod:`fonctions` module : fonctions pour la résolution du projet

:author: Roman Diez Berzal

:date: 2024 mars

"""
from PIL import Image


def charger_image(nom_fichier):
    """
    Charge une image depuis un fichier PNG ou CSV.
    """
    if nom_fichier.endswith('.png'):
        img = Image.open(nom_fichier)
        return img
    else:
        raise ValueError("uniquement utilisable avec un fichier png")
    
def affiche_image(img):
    """affiche une image en png

    Précondition : None
    Exemple(s) :
    $$$ 

    """
    charger_image(img).show()

def enregistrer_image(image, filename): 
    """
    Enregistre une image dans un fichier CSV.
    """

def decouper_image(image, ordre): 
    """
    Applique l'algorithme de découpe à une image avec un certain ordre (ordre à vérifier).
    """


def reconstituer_image(blocs): 
    """
    Reconstitue une image à partir de ses blocs.
    """
